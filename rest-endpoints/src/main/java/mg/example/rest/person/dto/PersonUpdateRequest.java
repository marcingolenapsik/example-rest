package mg.example.rest.person.dto;

import lombok.Data;

@Data
public class PersonUpdateRequest {
    private String description;
}
