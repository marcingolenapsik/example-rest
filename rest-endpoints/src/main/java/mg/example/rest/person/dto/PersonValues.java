package mg.example.rest.person.dto;

import lombok.Builder;
import lombok.Data;

import java.util.List;

@Data
@Builder
public class PersonValues {
    private List<String> personTypes;
}
