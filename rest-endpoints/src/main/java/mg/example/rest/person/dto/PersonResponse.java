package mg.example.rest.person.dto;

import lombok.Builder;
import lombok.Data;

@Data
@Builder
public class PersonResponse {
    private Person person;
    private PersonValues personValues;
}
