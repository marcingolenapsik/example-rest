package mg.example.rest.person.dto;

import lombok.Builder;
import lombok.Data;

import java.util.UUID;

@Data
@Builder
public class Person {
    private UUID exampleId;

    private String description;
    private String personType;
}
