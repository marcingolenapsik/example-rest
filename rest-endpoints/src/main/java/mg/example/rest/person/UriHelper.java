package mg.example.rest.person;

import lombok.experimental.UtilityClass;

import java.net.URI;
import java.util.UUID;

import static org.springframework.hateoas.mvc.ControllerLinkBuilder.linkTo;
import static org.springframework.hateoas.mvc.ControllerLinkBuilder.methodOn;

@UtilityClass
class UriHelper {
    static URI personByIdURI(UUID exitInterviewId) {
        return linkTo(methodOn(PersonEndpoint.class).getPerson(exitInterviewId)).toUri();
    }

    static URI allPeopleURI() {
        return linkTo(methodOn(PersonEndpoint.class).getAllPeople()).toUri();
    }
}
