package mg.example.rest.person;

import com.google.common.collect.Lists;
import lombok.val;
import mg.example.rest.person.dto.Person;
import mg.example.rest.person.dto.PersonResponse;
import mg.example.rest.person.dto.PersonValues;
import mg.example.rest.person.dto.PersonUpdateRequest;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;
import reactor.core.publisher.Flux;
import reactor.core.publisher.Mono;

import java.util.UUID;

@RestController
@RequestMapping("/person")
public class PersonEndpoint {

    @PostMapping
    public Mono<ResponseEntity> createPerson() {
        return Mono.just(
                ResponseEntity.created(UriHelper.personByIdURI(UUID.randomUUID())).build()
        );
    }

    @PutMapping("/{personId}")
    public Mono<ResponseEntity> updatePerson(@RequestBody PersonUpdateRequest updatePerson) {
        // TODO: should return OK and location
        return Mono.just(
                ResponseEntity.ok().build()
        );
    }

    @GetMapping("/{personId}")
    public Mono<PersonResponse> getPerson(@PathVariable UUID personId) {
        val data = Person.builder()
                .exampleId(personId)
                .description("desc: " + personId)
                .build();
        val values = PersonValues.builder()
                .personTypes(Lists.newArrayList())
                .build();

        return Mono.just(
                PersonResponse.builder()
                        .person(data)
                        .personValues(values)
                        .build()
        );
    }

    @GetMapping
    public Flux<PersonResponse> getAllPeople() {
        val personId1 = UUID.randomUUID();
        val personId2 = UUID.randomUUID();
        val data1 = Person.builder()
                .exampleId(personId1)
                .description("desc: " + personId1)
                .build();
        val data2 = Person.builder()
                .exampleId(personId2)
                .description("desc: " + personId2)
                .build();
        val values = PersonValues.builder()
                .personTypes(Lists.newArrayList())
                .build();

        return Flux.just(
                PersonResponse.builder()
                        .person(data1)
                        .personValues(values)
                        .build(),
                PersonResponse.builder()
                        .person(data2)
                        .personValues(values)
                        .build()
        );
    }
}
