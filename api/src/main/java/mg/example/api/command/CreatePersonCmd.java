package mg.example.api.command;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import org.axonframework.commandhandling.TargetAggregateIdentifier;

import java.util.UUID;

@Value
@Builder
@AllArgsConstructor
public class CreatePersonCmd {

    @TargetAggregateIdentifier
    private UUID personId;
}
